binary hero by phil
  
  
  Hardware part
  
  +5V--------------------------------------------------
     |      |      |      |      |      |      |      |  
     |      |      |      |      |      |      |      |     
     +-/    + /    + /    + /    + /    + /    + /    + /   
      /      /      /      /      /      /      /      /      
     /      /      /      /      /      /      /      /      
     |      |      |      |      |      |      |      |      
     +-D2   +-D3   +-D4   +-D5   +-D6   +-D7   +-D10  +-D11   
     |      |      |      |      |      |      |      |      
     -      -      -      -      -      -      -      -      
    |R|    |R|    |R|    |R|    |R|    |R|    |R|    |R|   
    |1|    |1|    |1|    |1|    |1|    |1|    |1|    |1|   
     -      -      -      -      -      -      -      -     
     |      |      |      |      |      |      |      |      
     |      |      |      |      |      |      |      |      
  GND--------------------------------------------------
  
  
  
  
  
  +5V------------------------------------
     |      |      |      |      |      |  
     |      |      |      |      |      |     
     +-/    +-/    + /    + /    + /    + /   
   [--/      /      /      /      /      /       
     /      /      /      /      /      /        
     |      |      |      |      |      |        
     +-D12  +-D13  +-A0   +-A1   +-A2   +-A3   
     |      |      |      |      |      |         
     -      -      -      -      -      -         
    |R|    |R|    |R|    |R|    |R|    |R|     
    |1|    |1|    |1|    |1|    |1|    |1|    
     -      -      -      -      -      -        
     |      |      |      |      |      |         
     |      |      |      |      |      |         
  GND------------------------------------
  
  
      
  D8-|    D8-|
     |       |
     _       _
    \ /     \ /
     -       -
     |       | 
     -       -  
    |R|     |R|      
    |2|     |3|     
     -       - 
     |       |
     |       |
     ---------
         |
         |----
         |   |
         |   -
         |  |R|
         |  |4|
         |   -
         |   |
         |   |----->To Gnd
        To   To
        RCA  RCA
        Pin  GND   
  
  ________________________________
  Pin|    Role      |Information |
  ________________________________
  D2 |Bit 2^0       |NC switch   |
  D3 |Bit 2^1       |NO switch   |        
  D4 |Bit 2^2       |NO switch   |     
  D5 |Bit 2^3       |NO switch   |   
  D6 |Bit 2^4       |NO switch   |   
  D7 |Bit 2^5       |NO switch   |    
  D8 |Tvout Video   |            |
  D9 |Tvout Sync    |            |
  D10|Bit 2^6       |NO switch   |
  D11|Bit 2^7       |NO switch   |
  D12|Help button   |NC switch   |
  D13|Pap button    |NC button   |
  A0 |nb bits select|NO switch   |
  A1 |nb bits select|NO switch   |
  A2 |speed select  |NO switch   |
  A3 |speed select  |NO switch   |
  ________________________________
  
  
  NO = Normaly open
  NC = Normaly close
  
  
  R1 = 10 Kohms
  R2 = 330 ohms
  R3 = 1 Kohms
  R4 = 75 ohms
  For leds I use basic red leds (I suppose you can use basic signal diode, but I haven't make the test...)
 
You need TVout lib on http://code.google.com/p/arduino-tvout/downloads/list
  
